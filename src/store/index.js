import Vue from 'vue'
import Vuex from 'vuex'
import Users from './Users'
import Stats from './Stats'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    Users,
    Stats
  }
})

export default store
