import Stats from '@/views/Stats/StatsHome'

export default [{
  path: '/Stats',
  name: 'StatsHome',
  title: 'Stats | Home',
  component: Stats
}]
