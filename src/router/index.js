import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'

import StatsRoutes from '@/router/Stats'

Vue.use(Router)

const baseRoutes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  }
]

const routes = baseRoutes.concat(StatsRoutes)

export default new Router({
  mode: 'history',
  routes
})
